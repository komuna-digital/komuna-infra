{ pkgs, ... }:

{
  imports = [ ./nginx.nix ];
  services.nginx.virtualHosts."dir.komuna.digital" = {
    enableACME = true;
    forceSSL = true;

    extraConfig = ''
      autoindex on;
      autoindex_exact_size off;
      autoindex_localtime on;
    '';

    locations."/" = {
      root = "/var/www/anuncios";
    };
  };
}
