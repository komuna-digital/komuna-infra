{ pkgs, ... }:

{
  services.komunews = rec {
    enable = true;
    baseAPIUrl = "komuna.digital";
    checkPeriod = 1800;
    home = "/var/komunews";
    accessTokenFile = "${home}/access_token";
    
    /* 
      adicione a esta lista a url para o feed RSS 
      da mídia que deseja adicionar 
    */
    feedsURLs = [
      "https://www.ofuturodesp.com.br/rss"
      "https://averdade.org.br/rss"
      "https://opoderpopular.com.br/rss/"
      "https://theintercept.com/feed/?lang=pt"
      "https://apublica.org/rss"
      "https://pt.granma.cu/feed"
      "https://www.brasildefato.com.br/rss2.xml"
      "https://revistaopera.com.br/rss"
      "https://mst.org.br/noticias/rss"
      "https://mtst.org/noticias/rss"
    ];
  };

}
