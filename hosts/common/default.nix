{ pkgs, ... }:

{
  imports = [ ../../users.nix ];
  
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
  };

  time.timeZone = "America/Sao_Paulo";

  fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [ "FiraCode" "CascadiaCode" ]; })
  ];


  environment.enableAllTerminfo = true;

  networking = {
    firewall.allowedTCPPorts = [ 22 ];
    nameservers = [ "8.8.8.8" "8.8.4.4" ];
  };

  services.openssh = {
    enable = true;
    settings = {
      PermitRootLogin = "no";
      PasswordAuthentication = false;
    };
  };

  programs.fish.enable = true;

}
