# Komuna Infra

Aqui está toda a configuração declarativa da infra da [komuna.digital](https://komuna.digital), usanod NixOS!

Você pode replicar essas configurações com

```
sudo nixos-rebuild switch --flake gitlab:komuna-digital/komuna-infra
```

## Adicionando feeds ao Komunews

Em [komunews.nix](https://gitlab.com/komuna-digital/komuna-infra/-/blob/main/hosts/sputnik/services/komunews.nix), adicione a url do feed que você deseja na opção `feedURLS`, para adicionar no komunews.
