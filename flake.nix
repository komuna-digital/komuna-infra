{

  inputs = {
    # Nixpkgs
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    nixos-hardware.url = "github:nixos/nixos-hardware/master";

    komunews = {
      url = "gitlab:komuna-digital/komunews";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";

  };

  outputs = { nixpkgs, nixos-hardware, flake-utils, komunews, ... }@inputs:
  let
      inherit (builtins) attrValues;
      inherit (flake-utils.lib) eachDefaultSystemMap;
    in
    rec {
      # Conjunto de mudanças à coleção de pacotes
      overlays = {
        # Nossas modificações
        default = import ./overlay;
        komunews = komunews.overlays.default;
      };


      # Exportar pacotes com todas as overlays aplicadas
      packages = eachDefaultSystemMap (system:
        import nixpkgs {
          inherit system;
          overlays = attrValues overlays;
          config.allowUnfree = true;
        }
      );

      # Configurações NixOS
      nixosConfigurations = {
        sputnik = nixpkgs.lib.nixosSystem {
          modules = [
            ./hosts/sputnik
            komunews.nixosModules.default
          ];
          specialArgs = { inherit inputs; };

          pkgs = packages.x86_64-linux;
          system = "x86_64-linux";
        };
      };
    };
}
